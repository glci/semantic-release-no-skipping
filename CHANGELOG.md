# 1.0.0 (2023-03-16)


### Bug Fixes

* corrects the docker commands in the README and adds the CI/CD for Semantic Release ([5ce49e8](https://gitlab.com/glci/semantic-release-no-skipping/commit/5ce49e8ddfbe2b70247848ba36ec91e7f077ecee))
* sets an alternative string for recognition by CI/CD pipelines `rules` ([fc625b3](https://gitlab.com/glci/semantic-release-no-skipping/commit/fc625b3d22fcd504f5b04956c3ae8c0ba7f91090))


### Features

* release of first version ([f4101be](https://gitlab.com/glci/semantic-release-no-skipping/commit/f4101be2968e6dc5042d07acac35b1326b7f90e6))
