# Semantic Release Docker image for GitLab CI (no skipping)

A docker image to enforce Semantic Versioning with the help of [semantic-release](https://semantic-release.gitbook.io/semantic-release/).

## Usage

As the Docker image is automatically build on GitLab, you can use it right in your pipeline:

```yaml
image: 'registry.gitlab.com/glci/semantic-release-no-skipping:latest'

cache:
  paths:
    - node_modules/

before_script:
  - npm i -G

...
```

## Development

To deploy on the container registry on GitLab: 
```shell
docker login registry.gitlab.com

docker build -t registry.gitlab.com/glci/semantic-release-no-skipping .
docker push registry.gitlab.com/glci/semantic-release-no-skipping
```
